# [blackbox-terminal](https://gitlab.gnome.org/raggesilver/blackbox) Packages

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-blackbox-terminal.asc https://packaging.gitlab.io/blackbox-terminal/gpg.key
```

## Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/blackbox-terminal/kinetic kinetic main" | sudo tee /etc/apt/sources.list.d/morph027-blackbox-terminal-kinetic.list
```

## Install packages

```bash
sudo apt-get update
sudo apt-get install blackbox-terminal morph027-keyring
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
echo 'Unattended-Upgrade::Origins-Pattern {"origin=morph027,codename=${distro_codename},label=blackbox-terminal";};' | sudo tee /etc/apt/apt.conf.d/50blackbox-terminal
```
