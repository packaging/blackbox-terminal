#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
  set -x
fi

export DEBIAN_FRONTEND=noninteractive
export CCACHE_DIR="${CI_PROJECT_DIR}/ccache"
mkdir -p "${CCACHE_DIR}"
tag="${CI_COMMIT_TAG:-v0.0.0+0}"
version="${tag%%+*}"
iteration="${tag##*+}"
tmpdir="$(mktemp -d)"
apt-get update
apt-get -y install \
    ccache \
    git \
    gettext \
    ruby-dev \
    valac \
    meson \
    cmake \
    desktop-file-utils \
    appstream-util \
    libgtk-4-dev \
    libadwaita-1-dev \
    libpqmarble-dev \
    libvte-2.91-gtk4-dev \
    libjson-glib-dev \
    libxml2-dev \
    librsvg2-dev \
    libgee-0.8-dev
git clone -b "${version}" https://gitlab.gnome.org/raggesilver/blackbox
cd blackbox
git apply "${CI_PROJECT_DIR}/patches/"*.diff
meson setup build --buildtype=release --prefix /usr
meson install -C build --destdir "${tmpdir}"
mkdir -p "${CI_PROJECT_DIR}/${CODENAME}"
gem install fpm
fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name blackbox-terminal \
    --package "${CI_PROJECT_DIR}/${CODENAME}/blackbox-terminal_${version//[a-zA-Z]/}-${iteration}_amd64.deb" \
    --version "${version//[a-zA-Z]/}" \
    --iteration "${iteration}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://gitlab.gnome.org/raggesilver/blackbox" \
    --description "A beautiful GTK 4 terminal." \
    --deb-recommends morph027-keyring \
    --depends libpqmarble2 \
    --depends libvte-2.91-gtk4-0 \
    --prefix "/" \
    --chdir "${tmpdir}" \
    --before-install "${CI_PROJECT_DIR}/.packaging/after-install.sh" \
